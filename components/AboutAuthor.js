import style from './AboutAuthor.module.css'

export default function AboutAuthor({ image, name, description, version, date, links=[] }) {
    const small = {
        border: 'none',
        background: 'none',
        color: 'var(--primary)',
        fontSize: '0.65em',
        textTransform: 'uppercase',
        textDecoration: 'none',
        marginRight: '1em'
      }
    return (
        <div className="linkHold">
            <div className={style.authorFlex}>
                <div className={style.profileHold}>
                    <img src={image} className={style.imgsize}/>
                </div>
                <div>
                <div className={style.versionFlex}>
                    { version ? (
                    <p className={style.sansSerif}>
                        {'v '+version}
                    </p>
                    ) : (
                        null
                    )
                    }
                    <p className={style.sansSerif}>
                        {date}
                    </p>
                </div>
                
                </div>
            </div>
            <h5 style={{marginBottom: '1em'}}>{name}</h5>
            <p className={style.sansSerif}>{description}</p>
            {/*}
            { links[0] ? (
            links.map(({name, target}, index) => (
                <a key={index} href={target} style={small}>{name}</a>
             ))
            ) : (
                null
            )
            }*/}
        </div>
    )
}