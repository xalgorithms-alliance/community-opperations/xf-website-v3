import { Button } from 'xf-material-components/package/index'
import Persona from './Persona'
import style from './Graphics.module.css'
import ImageFullScreen from './ImageFullScreen'

export default function Graphics() {
    return(
        <div class={style.graphicHold} >
            <div>
                <h2 className={style.t} style={{'padding-bottom': '10px'}}> Concepts And Operations</h2>

                <ImageFullScreen src="/ConceptsAndOperations/DWDS-concepts&operations.png" width='70vw'/>
            </div>
            <div style={{'display': 'flex',  gap: '20px', 'padding-bottom' : '5px'}}>
                <div>
                    <h5 class={style.t}> RuleMaker </h5>
                <ImageFullScreen src="/ConceptsAndOperations/rm_sequence_diagram.png" width='30vw'/>
                </div>
                <div>
                    <h5 class={style.t}> RuleReserve </h5>
                <ImageFullScreen src="/ConceptsAndOperations/rr_sequence_diagram.png" width='30vw'/>
                </div>
                <div>
                    <h5 class={style.t}> RuleTaker </h5>
                <ImageFullScreen src="/ConceptsAndOperations/rt_sequence_diagram.png" width='30vw'/>
                </div>
            </div>
        </div>
    )
}