import Link from "next/dist/client/link"
import style from './NavGetInvolved.module.css'
import Usecase from './Usecas'


export default function NavWriting() {
    return(
        <div className={style.triflex}>
            <div id={style.wide}>
            <p className={style.labelb}>Featured Posts</p>
            <br />
            <div>
                <Usecase title="" cta="Which Way is Forward?" image="/writing-images/direction.png" target="/writing/which-way-is-forward"/>
                <br />
                <Usecase title="" cta="The Data With Direction Specification (DWDS) in a Nutshell" image="/writing-images/RULE-OUGHT.png" target="/writing/dwds-in-a-nutshell"/>
                
                </div>
                </div>
                <div className={style.hline}></div>
                <div>
                    <Link href="/writing"><a className={style.a}>All Blog Posts</a></Link>
                </div>
            
            {/*}
            <div className={style.hrule}/>
            <div className={style.spacer} />
            <a className={style.a} id={style.yesmargin}>Publications</a>
            <p className={style.labelb}>Featured Publications</p>
            <div className={style.personagrid}>
                <div className={style.flexdown}>
                    <a className={style.a}>Publication 1</a>
                    <a className={style.a}>Publication 2</a>
                    <a className={style.a}>Publication 3</a>
                </div>
                <div className={style.flexdown}>
                    <a className={style.a}>Publication 4</a>
                </div>
            </div>
    */}
        </div>
    )
}