export default function sortTags(data) {
    const all = data.map(({published, tags=[]}) => {
        published ? (
            tags.map(({tag}) => {
            //setTagArray(tagArray => tagArray.concat(tag))
                return {
                    tag
                }
            }) 
        ) : (
            null
        )
    })
    return {
        all
    }
}