const withMDX = require('@next/mdx')()
module.exports = withMDX()

/*
module.exports = {
  async redirects() {
    return [
      {
        source: '/white-paper',
        destination: 'https://gitlab.com/xalgorithms-alliance/dwds-documents/-/tree/master/current',
        permanent: true,
      },
    ]
  },
}
*/
