import Head from 'next/head'
import SEO from '../components/SEO'

export default function donate() {
    return (
      <>
        <SEO title="The Xalgorithms Foundation | Donate" description="A community building a simple, scalable, free open source way to publish, discover, fetch, scrutinize and prioritize rules-as-data." img="seo.png"/>
        <div className="container">
          <Head>
            <title>Xalgorithms Foundation</title>
            <link rel="icon" href="/favicon.ico" />
          </Head>
          <main>
          <section>
          <div className="communityFlex"> 
          <div style={{maxWidth: "600px", margin: "auto", paddingTop: "0em", paddingBottom: "4em"}}>
              <h1>
                  Donate
              </h1>
              <p>
              How to Contribute Research Funds
              <br />
              We invite financial contributions to help advance the Data With Direction Specification, the reference implementation, service deployment, and particular use-case working groups. Payment can be made via direct deposit to:
              <br />
              <br />
Xalgorithms Foundation
<br />
50 Hines Road, Suite 240
<br />
Ottawa, Canada K2K 2M5
<br />
Reserve Account #1079433
<br />
Transit #00006
<br />
Institution #003
<br />
SWIFT #ROYCCAT2
<br />
<br />
Please contact <a href="mailto:jpotvin@xalgorithms.org">Joseph Potvin</a>, Executive Director to make alternative contribution arrangements, and to receive a contribution receipt.
              </p>
              </div>
              </div>
              </section>
          </main>
        </div>
      </>
    )
  }