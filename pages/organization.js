import TitleMod from '../components/TitleMod'
import FoundationCall from '../components/FoundationCall'
import DropDown from '../components/DropDown'
import Timeline from '../components/Timeline'
import Link from 'next/link'

import { useContext } from 'react';
import communityContent from '../content/communityContent'
import LanguageSelect from '../lib/languageSelect'
import AboutAuthor from '../components/AboutAuthor'
import SEO from '../components/SEO'

export default function Organization() {
    const lang = useContext(LanguageSelect)
    const content = communityContent[lang]
    const renderTeam = () => {
        return content.team.map(({img, name, bio, links}, index ) => (
          img ? (
          <div className="indexWritingChild" key={index}>
            <AboutAuthor image={img} name={name} description={bio} links={links}/>
          </div>
          ) : (
            null
          )
        ));
      }
    return(
        <>
        <SEO title="Organization" description="A community building a simple, scalable, free open source way to publish, discover, fetch, scrutinize and prioritize rules-as-data." img="seo.png"/>
        <div style={{height: '6em'}} />
            <div style={{height: '6em'}} />
            <TitleMod title="A Decentralized Collaborative Community" img='community.png'/>
            <div className="holdingteampreview">
                {renderTeam()}
            </div>
            
            {/*
            <Timeline />
            */}
            {/*}
            <DropDown title="About the Org" content="lorem ipsum" preview="beep" id="about"/>
    <DropDown title="Funding" content="lorem ipsum" preview="beep" d="funding"/>*/}
            <DropDown title="Xalgorithms Alliance" id='alliance'>
              <p>
              Xalgorithms Foundation Inc. is a not-for-profit corporation operating globally from Ottawa, Canada. The sole purpose of the Foundation is to provide services to the Xalgorithms Alliance of organizational and individual Members. Through Working Groups, participants in the Alliance are advancing functional free/libre/open source components and use cases. Xalgorithms Alliance has five categories of Members: (1) Strategic Implementers, (2) Experimenting Implementers, (3) Developers & Researchers, (4) R&D Donors, and (5) External Advisors. <Link href="https://www.xalgorithms.org/members" target="_blank">here</Link>.
    <br />
    <br />
Xalgorithms Working Groups convene business, government, academic and/or not-for-profit organizations, as well as informal civil society communities and interested individuals, to solve a category of problem. Each Working Group operates for the purposes and within the scope documented in their written charter.
<br />
<br />
For full details, the Xalgorithms Alliance Accession Agreement (XAAA) can be <a href="https://gitlab.com/xalgorithms-alliance/data-with-direction-specification/xalgorithms_alliance_and_accession_agreement/-/blob/main/Xalgorithms_Alliance_and_Accession_Agreement_TEMPLATE_2024-05-10PDF.pdf?ref_type=heads" target="_blank">found here</a>
.
<br />
<br />
To join the Alliance, contact Xalgorithms Executive Director, <a href="mailto:jpotvin@xalgorithms.org">Joseph Potvin</a>.


              </p> 
            </DropDown>
            <DropDown title="The Value Proposition for Participating in Free Libre Open Works" id='value'>

              <p>Free/libre/open works involve multi-organizational joint or collective authorship and distribution of re-usable digital data, information or knowledge works, which the parties each consider to be helpful towards achieving their business goals. They put in place arrangements across organizational boundaries to collectively self-supply specific solutions that meet their respective goals.&nbsp;</p><p>Each cooperating individual or team within the project community allocates resources for collective use when the benefits, risk management and/or cost outcomes of this collaboration are more attractive than what they could achieve via a solo implementation, or through any other available option. No individual or team would have a business incentive to participate in the creation or maintenance of a free/libre/open work unless the outcome were more:&nbsp;</p><ul><li><strong>Cost effective:</strong> If equivalent outcomes can be obtained by some other route that takes less time and money, then the other option should be engaged. <br />or&nbsp;</li><li><strong>Cost efficient:</strong> Net outcome improvements from participation should be justifiable in view of relative amount of time and money require for participation.&nbsp;</li></ul><p>Many managers have reported significant benefits through collective self-provisioning through open source methods:</p><blockquote><p><em>&nbsp;• "Saved us $120K this year, but several hundred thousand dollars in time savings";</em><br /><em>&nbsp;• "We have seen a 75% cost savings, and we have significantly advanced our time­lines";</em><br /><em>&nbsp;• "What would have taken us two years to put into place, we accomplished in three weeks";</em><br /><em>&nbsp;• "It is unbelievably cost effective by as much as a factor of 10".</em></p></blockquote><div class="wikimodel-emptyline"></div><p>These results are consistent with a view of ethical business as an optimal form of rationality. (See pg 12 in: <em>Economic rationality and ethical behavior</em> <span ><a href="http://ideas.repec.org/p/upf/upfgen/584.html"><span >http://ideas.repec.org/p/upf/upfgen/584.html</span></a></span> ) The set of economic purposes served, and the value proposition for organizations, is to enable participating projects, services and their partners to augment benefits, better manage risk, and reduce costs through collaborative provisioning and inter-organizational learning, across boundaries in cooperation with other teams and organizations.&nbsp;</p><h4 ><span>Augment Benefits</span></h4><ul><li>Optimize the pace of innovation through inter-organizational collaboration and learning:<ul><li>International&nbsp;</li><li>Cross-sector/Cross-departmental&nbsp;</li><li>Cross-industry&nbsp;</li></ul></li><li>Leverage existing intellectual and informatics works that have already been paid for&nbsp;</li><li>Foster an agile and competitive environment by reducing barriers to entry&nbsp;</li><li>Strengthen in-house and/or independent security, management and financial control&nbsp;</li><li>Diversify and decentralize opportunities for employment and business<ul><li>Customization for niche requirements&nbsp;</li><li>Opportunities for small/medium enterprise outside major cities&nbsp;</li></ul></li><li>Engage internal and external expertise<ul><li>Different core competencies&nbsp;</li><li>Quality assurance community&nbsp;</li><li>Implementation community&nbsp;</li></ul></li></ul><h4 ><span>Manage Risk</span></h4><ul><li>Distribute risk amongst multiple participant-tenants</li><li>Advance and protect the "knowledge commons"&nbsp;</li><li>Outlast team/organization (sustainability)‏&nbsp;</li><li>Learn from peer review feedback<ul><li>Praise and/or criticism&nbsp;</li><li>Confirmation/rejection of assumptions&nbsp;</li><li>Personnel retention &amp; succession management&nbsp;</li></ul></li></ul><h4 ><span>Reduce Costs‏</span></h4><ul><li>Reuse components (own &amp; others)‏<ul><li>Redirect creativity, effort and spending out of redundancy and idiosyncrasy&nbsp;</li><li>Externalize some costs of creation/evolution&nbsp;</li><li>Retain the freedom to stand still (no forced obsolescence/upgrades)&nbsp;</li></ul></li><li>Reduce time and money required for legal reviews<ul><li>Reduce or eliminate the requirement for “non-disclosure agreements”&nbsp;</li><li>Simplify copyright license management&nbsp;</li><li>Rely upon general purpose re-usable licenses&nbsp;</li></ul></li><li>Reduce start-up and delivery times<ul><li>Engage international standards by default&nbsp;</li><li>Align to a more elegant modular architecture&nbsp;</li><li>Allow for more agile systems development&nbsp;</li><li>Retain freedom to adapt to requirements</li></ul></li></ul>
  
            </DropDown>
            <DropDown title="Explore the Free/Libre/Open Way" id="open">
            <p>Xalgorithms Alliance arranges all of its relationships and operations with the demand-side perspective of The Free Software Definition (i.e. user freedoms), and the supply-side perspective of The Open Source Software Definition (i.e. developer principles).</p>
<h4 >Demand-Side Perspective</h4>
<p><a href="https://gnu.org/philosophy/free-sw.html" target="_blank">The Free Software Definition</a>: A computer program is distributed free/libre when anyone who obtains it retains the following freedoms:</p>
<ul>
<li>Freedom 0: Freedom to run the program for any purpose.</li>
<li>Freedom 1: Freedom to study how the program works, and adapt it to one’s needs. Unencumbered access to the source code is a precondition for this.</li>
<li>Freedom 2: Freedom to copy the program and to redistribute copies.</li>
<li>Freedom 3: Freedom to improve the program, and release any modified versions. Unencumbered access to the source code is a precondition for this.</li>
</ul>
<h4 data-fontsize="18" data-lineheight="27">Supply-Side Perspective</h4>
<p><a href="http://opensource.org/osd-annotated" target="_blank">The Open Source Software Definition</a>: A computer program is distributed open source when everyone distributing it abides by the following principles:</p>
<ul>
<li>Permit free redistribution</li>
<li>Publish source code</li>
<li>Welcome derivative works</li>
<li>Respect integrity of author’s source code</li>
<li>Ensure the license is technology-neutral</li>
<li>Do not discriminate against persons or groups</li>
<li>Do not discriminate against fields of endeavour</li>
<li>Do not link with non-disclosure agreements</li>
<li>Do not tie the license to a particular product</li>
<li>Do not restrict other software’s terms and conditions</li>
</ul>
<h4>Isn’t “open source” always “free/libre”?</h4>
<p>No. Permissive licenses such as those known as “MIT” and “Apache” are open source, but not free/libre. Programming code under these licenses can be relicensed by anyone directly or as derivative works under a restrictive license, which means that free/libre terms and conditions are not in force.</p>
<p>The open source hybrid “Eclipse Pubic License” allows its open source code to be intermingled with code under restrictive licensing. So a mixed package under Eclipse does not meet free/libre terms and conditions.</p>
<p>The various “GNU” licenses are both free/libre AND open source. Programming code under a GNU software license may only be intermingled and distributed with code under other free/libre/open licenses. These licences are one-way compatible with permissive licences like MIT and Apache, meaning that code under open source licences can be re-licensed and distributed under the free/libre/open GNU licenses.</p>
<p>Most of the global free/libre and open source market uses GNU licenses. Therefore what is colloquially called “open source” software is usually “free/libre/open source” software. That overlap can lead to some confusion, but it’s helpful to be clear in our language about these terms of trade over intellectual rights and responsibilities. There’s no need to dwell on these details if they distract from the purpose at hand. Suffice to say, the free/libre/open branding does pack a whole lot of significance.</p>
<p>Xalgorithms Foundation is explicitly a free/libre/open source initiative. We distribute our work “open source” under the Apache 2.0 for the programming code that is <strong>intended for ubiquitous deployment</strong>, including within solutions under restrictive licensing. And we distribute our work “free/libre” under the GNU GPL 3.0 when <strong>intended to be maintained free/libre</strong>. The different license have different purposes.</p>
            </DropDown>
            <FoundationCall />
            
        </>
    )
}
