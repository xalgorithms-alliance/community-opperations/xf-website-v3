---
title: 'How to Help'
date: '2021-06-23'
featimg: 'writing-images/how-tohelp.png'
featimgalt: ''
summary: ''
author: 'Joseph Potvin'
authorProfile: ''
authorDescription: ''
tags: [
    {tag: 'free software' },
    {tag: 'open source'},
    {tag: 'flos'}
]
version: '1.0'
published: 'true'
---


On a ship, train or spacecraft where each crew member has a role to perform, an essential part of that role is to actively help everybody else. 

Similarly, the way to put yourself on-board with any free/libre/open source project, is just to start helping on some aspect you're capable in. You'll quickly get to know some of the crew, and how things work, and where we're heading next. If its a project with multiple parts or diverse use cases, chances are your involvement can be readily adapted to your existing know-how, as well as to your motivation to learn new capabilities towards your own longer-term goals.

That's how it is with the Xalgorithms community. 

Our mission is to make it easy for anyone to discover and obtain factual knowledge of the rules that are in effect for, applicable to, and invoked by any circumstance at any given moment. We have designed a general-purpose request-response specification to mobilize rules-as-data across digital networks, with precision, simplicity, scale, volume and speed, and with deference to any jurisdiction's meta-rules about rule-making (prerogative). 

If we're successful, just one small step for users should be one giant leap for trade and commerce, mechatronics, money and finance, ecological management, industrial control systems, digital learning, and other domains where the communication of obligation, permission and encouragement could use a boost. 

We're building three reference implementations on different platforms. You'll find our in-progress software components on <a href="https://gitlab.com/xalgorithms-alliance" target="_blank">GitLab</a> Our system design, or a derivative of it, can be re-implemented independently by anyone else on their preferred platform.

There are some conceptual and design aspects of this project that are difficult to get one's head around, not because they are too daunting, but because they are too ordinary. Christopher Alexander addresses this quirk of design in his famous book, *The Timeless Way of Building*. He says that the most pivotal design insights are typically the most difficult to perceive “because they are so ordinary, that they strike to the core ... What makes them hard to find is not that they are unusual, strange or hard to express―but on the contrary that they are so ordinary”.  

Our trajectory has surprised us a few times! Back in 2015 we picked a spot in the distance, and have not wavered. But to get there has been a non-linear voyage of discovery, not a mapped journey. 

Amongst our volunteers, our contractors, and our funders, this is an agent-based community of self-motivated, self-directed, mutually helpful people. We blend measured amounts of pure pragmatism and raw creativity to generate forward propulsion, and we maintain momentum by building and deploying genuine working software, network services, and data structures.   

If you're suited to this sort of expedition, here are a few suggestions to get your bearings:

* Identify a part of the project, or a type of use case, that's especially interesting to you, and launch from that. For example, Craig Atkinson has been pursing the domain of cross-border trade. Deja Newton and Danielle Derrick are contributing to applied ecosystem science. Building on the previous work of Ryan Fleck, Huda Hussain is focusing on the RuleMaker application. Bill wants to transform fiscal policy instruments from dumb to smart. Jacob Kelter, Jake Wit, Amanda Sugiharto and Will Conboy brought agent-based modeling into the mix. And so on. What mission are *you* in pursuit of?

* Let others know what you'd like to accomplish: write a reflective, substantive, exploratory blog about it for our site. Discuss through our email lists and social media venues. Somebody will join in to help. If you're feeling uncertain about how to connect the dots, welcome to the club! Here's [a self-explanatory diagram](https://wiki.opensource.org/bin/download/Working%20Groups%20%26%20Incubator%20Projects/flow-syllabus/The%20General%20Direction%20of%20FLOW/The%20Methodological%20Significance%20of%20Uncertainty/WebHome/uncertainty_visualize_medium.png?rev=1.1) that I adapted in 1992 for Global Environment Facility, building on a thoughtful article by Adrian McDonough, former chair of the “Committee on NASA Information Systems”. We're all in there, somewhere.

* Allocate time to read the paper: *DWDS*. It provides the underlying rationale for what DWDS is intended to accomplish, and how it is intended to work. If you reckon you can sum it up well in 5 or 10 pages, that would be appreciated!  In relation to this present blog post, see the section on "design virtues" and "design principles". Our design virtues are: Human-Centred Automation; Free/Libre/Open Relationships; Tolerance; Interoperability. And our design principles are: Simplicity; Intuitiveness; Decentralization; Modularity; Least Power. Please help to keep us on track with those. 

* Take into account how Xalgorithms is funded, structured, coordinated and licensed. The 'living' document *Xalgorithms Foundation Operational Framework* outlines these project management aspects. Xalgorithms Foundation is an incorportated not-for-profit, and the software we produce is 100% free/libre/open source in both its source code and its dependencies. Our start-up was enabled by team sweat equity and private-sector funding from Xalgorithms Alliance charter member [DataKinetics](https://dkl.com/company/). Please read the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0.html) that we use for our implementations of RuleMaker and RuleTaker, and read the [GNU Affero GPL 3.0 license](https://www.gnu.org/licenses/agpl-3.0.en.html) used for our implementations of the RuleReserve software. It would also be a good idea to read the full version of the [Creative Commons Attribution license](https://creativecommons.org/licenses/by/2.0/legalcode) under which we share our documentation and graphics.

*  We invite financial contributions to help advance DWDS, the reference implementations, service deployment, and particular use-case working groups. Please contact [Joseph Potvin](mailto:jpotvin@xalgorithms.org), Executive Director to discuss contribution arrangements and a contribution receipt. 

<a href="/donate">
<Button>Make a Contribution</Button>
</a>
